from selenium.webdriver import Firefox, PhantomJS
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from scraper.html_profiles import resmatrix, sabre
from scraper.loader import BookingLoader
from booking.models import Booking
from utils.date import get_month_date_ranges
from urllib import parse
import os
import pickle
import configparser
import time
import logging
import secrets
from datetime import datetime

scripter_log = logging.getLogger("lbs")
scripter_log.setLevel(logging.INFO)

cfg = configparser.ConfigParser()

scraper_settings_file = "settings.ini"
driver_file_name = "geckodriver"
browser_class = PhantomJS

ROOT_SCRAPER_FOLDER = os.path.dirname(__file__)
DRIVER_FOLDER = os.path.join(ROOT_SCRAPER_FOLDER, "webdrivers")

CONFIG_SECT_URL_PARAM = "URL_Params"
SETTINGS_FILE = "settings.ini"

BETWEEN_PAGE_WAIT = 10


class NoURLQueryException(Exception):
    pass


class NoElementFoundException(Exception):
    pass


class NotValidSpecialXpath(Exception):
    pass


class PageChangeEvent:
    old_url = ""

    def __init__ (self, old_url):
        self.old_url = old_url

    def __call__(self, driver):
        return driver.current_url != self.old_url


class PageDirectEvent:
    old_url = ""

    def __init__(self, old_url):
        self.old_url = old_url.strip("/")

    def __call__(self, driver):
        return driver.current_url.strip("/") == self.old_url.strip("/")


class ElementExistsByIDEvent:
    ele_id = None

    def __init__(self, element_id):
        self.ele_id = element_id

    def __call__(self, driver):
        return len(driver.find_elements_by_id(self.ele_id)) > 0


class ElementExistsEvent:
    xpath = None

    def __init__(self, xpath):
        self.xpath = xpath

    def __call__(self, driver):
        return len(driver.find_elements_by_xpath(self.xpath)) > 0


class ElementUpdatedEvent:
    original_value = ""
    xpath = ""

    def __init__(self, original_value, xpath):
        self.original_value = original_value
        self.xpath = xpath

    def __call__(self, driver):
        return driver.find_element_by_xpath(self.xpath).text != self.original_value


XTCR_OPT_DATE_FORMAT = "date_format"
XTCR_OPT_DATETIME_FORMAT = "datetime_format"


class CRSCrawlerResult:
    def __init__(self):
        self.new = 0
        self.updated = 0


class DataExtractor:
    loader = None

    cfg_section = None
    crawler_cfg = configparser.ConfigParser()

    def __init__(self, loader: BookingLoader):
        self.crawler_cfg.read(os.path.join(ROOT_SCRAPER_FOLDER, SETTINGS_FILE))
        self.date_format = self.crawler_cfg.get(self.cfg_section, XTCR_OPT_DATE_FORMAT, fallback=None)
        self.datetime_format = self.crawler_cfg.get(self.cfg_section, XTCR_OPT_DATETIME_FORMAT, fallback=None)

        self.loader = loader
        self.loader.date_format = self.date_format
        self.loader.datetime_format = self.datetime_format

CRWLR_OPT_USERNAME = "username"
CRWLR_OPT_COOKIE_FILE = "cookie_file"
CRWLR_OPT_LOGIN = "login_page"
CRWLR_OPT_BASE_RESULT = "base_result_page"
CRWLR_OPT_USERNAME_ELEMENT_ID = "username_element_id"
CRWLR_OPT_PASSWORD_ELEMENT_ID = "password_element_id"
CRWLR_OPT_SUBMIT_BTN_ID = "submit_btn_id"
CRWLR_OPT_SESSION_DATA_FILE = "session_data_file"


class LegacyCRSCrawler(DataExtractor):
    html_profile = None
    FOLDER = os.path.join(ROOT_SCRAPER_FOLDER, "session-settings")

    save_cookies = True
    _booking_method = None

    def __init__(self, loader):
        super().__init__(loader)
        self.username = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_USERNAME, fallback=None)
        self.cookie_file = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_COOKIE_FILE, fallback=None)
        self.login_page = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_LOGIN, fallback=None)
        self. base_result_page = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_BASE_RESULT, fallback=None)
        self.session_data_file = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_SESSION_DATA_FILE, fallback=None)
        self.username_element_id = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_USERNAME_ELEMENT_ID, fallback=None)
        self.password_element_id = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_PASSWORD_ELEMENT_ID, fallback=None)
        self.submit_btn_id = self.crawler_cfg.get(self.cfg_section, CRWLR_OPT_SUBMIT_BTN_ID, fallback=None)

    def build_target_url(self):
        if self.session_data_file:
            with open(self.get_session_data_filepath(), 'r') as stngfile:
                cfg.read_file(stngfile)
                param_str_lst = [t[0] + "=" + t[1] for t in cfg[CONFIG_SECT_URL_PARAM].items()]
                return "{}?{}".format(self.base_result_page, "&".join(param_str_lst))
        else:
            return self.base_result_page

    def get_cookie_filepath(self):
        return os.path.join(self.FOLDER, self.cookie_file)

    def get_session_data_filepath(self):
        if self.session_data_file:
            return os.path.join(self.FOLDER, self.session_data_file)
        return False

    def has_cookie_file(self):
        return os.path.exists(os.path.join(self.FOLDER, self.cookie_file))

    def has_session_data_file(self):
        return os.path.exists(os.path.join(self.FOLDER, self.session_data_file))

    def login(self, login_browser, login_data, save_after_success=False):
        login_browser.get(self.login_page)
        WebDriverWait(login_browser, BETWEEN_PAGE_WAIT).until(PageDirectEvent(self.login_page))
        if self.has_cookie_file() and self.save_cookies:
            scripter_log.error("Cookie exists. Bypassing login")
            cookie_file = self.get_cookie_filepath()
            for c in pickle.load(open(cookie_file, 'rb')):
                try:
                    login_browser.add_cookie(c)
                except WebDriverException as e:
                    scripter_log.error("Couldn't load cooking {} ".format(c))
            target_url = self.build_target_url()
            login_browser.get(target_url)
            try:
                WebDriverWait(login_browser, BETWEEN_PAGE_WAIT).until(PageDirectEvent(target_url))
            except TimeoutException:
                os.remove(cookie_file)
                scripter_log.warn("Cookie is Invalid. Deleted Cookie and preparing manual login")
                self.login(login_browser, login_data.password)
        else:
            scripter_log.error("No Cookie exists. Logging in...")
            if not self.has_cookie_file() and self.save_cookies:
                scripter_log.error("Missing login settings. Needs manual log in.")
            else:
                scripter_log.error("Logging in using login settings.")
                lgn_cfg = configparser.ConfigParser()
                lgn_cfg.read(os.path.join(ROOT_SCRAPER_FOLDER, scraper_settings_file))
                login_browser.get(self.login_page)

            WebDriverWait(login_browser, BETWEEN_PAGE_WAIT).until(PageDirectEvent(self.login_page))
            old_url = login_browser.current_url
            usrname_tbox = login_browser.find_element_by_id(self.username_element_id)
            usrname_tbox.send_keys(login_data.username)
            password = login_browser.find_element_by_id(self.password_element_id)
            password.send_keys(login_data.password)

            if self.submit_btn_id:
                login_browser.find_element_by_id(self.submit_btn_id).click()
            else:
                password.submit()

            try:
                WebDriverWait(login_browser, BETWEEN_PAGE_WAIT).until(PageChangeEvent(old_url))
            except TimeoutException as e:
                scripter_log.error("Timeout by Failed Login. {} doesn't change.".format(login_browser.current_url))
                scripter_log.error(e)

            if save_after_success:
                self.save_login_data(login_browser)

    @classmethod
    def update_bookings(cls, start_date, end_date):
        raise NotImplementedError

    def save_login_data(self, browser):
        pickle.dump(browser.get_cookies(), open(self.get_cookie_filepath(), "wb"))
        if self.session_data_file and not self.has_session_data_file():
            cfg.add_section(CONFIG_SECT_URL_PARAM)
            for pramtr in parse.urlparse(browser.current_url).query.split('&'):
                name, var = pramtr.split('=')
                try:
                    cfg.set(CONFIG_SECT_URL_PARAM, name, var)
                except ValueError:
                    scripter_log.error("URL contains no query paraments. ")

            with open(self.get_session_data_filepath(), 'w+') as prmfile:
                cfg.write(prmfile)


RESMATRIX_LINK_ID = "CPH1_rpResvList_hlResvID"
RESMATRIX_PAGER_DIV_ID = "CPH1_AspNetPager1"


class ResMatrixLegacyCrawler(LegacyCRSCrawler):
    login_data = secrets.ResMatrix
    unimportant_fields = ["Guest Comment", "Cancellation Policies", "Travel Agent Phone#", ]
    RMTX_ID_REGEX = "(?<=ResvID\=)[0-9]+"
    RMTX_RES_DET_LINK_TEMPLATE = "https://res.travlynx.com/Admin/Resv/Resv_Detail_Print.aspx?ResvID={}"
    RESERVATIONS_PER_PAGE = 10
    DATE_INPUT_FORMAT = "%m/%d/%Y"
    MAX_DATE = "12/31/2070"
    cfg_section = "ResMatrix"

    def update_bookings(self, start_date, end_date):
        res = CRSCrawlerResult()

        if isinstance(start_date, datetime):
            new_start_date = start_date.strftime(self.DATE_INPUT_FORMAT)
        else:
            new_start_date = start_date

        if isinstance(end_date, datetime):
            new_end_date = end_date.strftime(self.DATE_INPUT_FORMAT)
        else:
            new_end_date = end_date

        brsr = Firefox()

        self.login(brsr, self.login_data, save_after_success=False)

        brsr.get(self.build_target_url())

        bdate = brsr.find_element_by_xpath("//input[@id='CPH1_txtBeginDate']")
        bdate.clear()
        bdate.send_keys(new_start_date)
        edate = brsr.find_element_by_xpath("//input[@id='CPH1_txtEndDate']")
        edate.clear()
        edate.send_keys(new_end_date)

        brsr.find_element_by_id("CPH1_btnSearch").click()
        pager_id = "ctl00$CPH1$AspNetPager1_input"

        try:
            WebDriverWait(brsr, 5).until(ElementExistsByIDEvent(pager_id))
        except TimeoutException:
            last_page_num = 1
            scripter_log.warn("Dropdown Not detected. Assuming only one page exists.")
        else:
            last_page_num = int(brsr.find_element_by_id(pager_id).find_elements_by_xpath("./option")[-1].text)

        for page_num in range(1, last_page_num+1):
            scripter_log.warn("Scraping Page {}".format(page_num))
            time.sleep(2)
            for link_num in range(self.RESERVATIONS_PER_PAGE):
                res_link_id = "{}_{}".format(RESMATRIX_LINK_ID, link_num)
                try:
                    res_link_btn = brsr.find_element_by_id(res_link_id)
                except NoSuchElementException:
                    break
                res_id = res_link_btn.text
                if 0 == len(Booking.objects.filter(crs_id=res_id)):
                    res_link_btn.click()
                    try:
                        WebDriverWait(brsr, 5).until(ElementExistsEvent("//iframe"))
                    except TimeoutException:
                        res_link_btn.send_keys(Keys.PAGE_UP)
                        time.sleep(1)
                        res_link_btn.click()
                        WebDriverWait(brsr, 5).until(ElementExistsEvent("//iframe"))

                    pop_up = brsr.find_element_by_xpath("//iframe")
                    brsr.switch_to.frame(pop_up)
                    WebDriverWait(brsr, 10).until(ElementExistsEvent("//a[text()='Itinerary']"))

                    profile = resmatrix.ResMatrixCRSProfile()
                    reservation_details = {}
                    for tab_name in profile.html_pages.keys():
                        try:
                            brsr.find_element_by_link_text(tab_name).click()
                        except (ElementNotVisibleException, NoSuchElementException):
                            if tab_name not in ['Guarantee', 'Restrictions']:
                                scripter_log.warn("Tab: {} is not visible".format(tab_name))
                        field_list = profile.html_pages[tab_name]
                        for element_id, field_name in field_list:
                            try:
                                reservation_details[field_name] = brsr.find_element_by_css_selector(element_id) \
                                    .text.replace("\n", "")
                            except NoSuchElementException:
                                if field_name not in ['pnr']:
                                    scripter_log.warn("Field, {}, doesn't exists for {}".format(field_name, res_id))
                            else:
                                if not reservation_details[field_name] and field_name not in self.unimportant_fields:
                                    time.sleep(1)
                                    reservation_details[field_name] = brsr.find_element_by_css_selector(element_id) \
                                        .text.replace("\n", "")
                    bk_id = reservation_details.get('pk', '')
                    self.loader.load_booking(reservation_details)
                    res.new += 1
                    brsr.find_element_by_xpath("//body").send_keys(Keys.ESCAPE)
                    brsr.switch_to.default_content()
                else:
                    scripter_log.warn("Booking {} already exists".format(res_id))
                time.sleep(1)
            next_page = page_num + 1
            if next_page <= last_page_num:
                first_res_id = brsr.find_element_by_id("{}_0".format(RESMATRIX_LINK_ID)).text
                next_option = brsr.find_element_by_id(RESMATRIX_PAGER_DIV_ID)\
                    .find_element_by_xpath("//a[text()='>']")
                next_option.click()
                WebDriverWait(brsr, 10).until(ElementUpdatedEvent(
                    first_res_id, "//a[@id='{}_0']".format(RESMATRIX_LINK_ID)),
                                              "First Reservation Hasn't Updated")
                brsr.find_element_by_tag_name("body").send_keys(Keys.HOME)
                time.sleep(2)
        brsr.quit()

        return res


class CRSCrawler(DataExtractor):
    login_data = None
    crs_profile = None
    FOLDER = os.path.join(ROOT_SCRAPER_FOLDER, "session-settings")

    def __init__(self, loader):
        super().__init__(loader)

    def login(self, login_browser, save_login_data=False):
        lgn_page = self.crs_profile.login_page
        login_browser.get(lgn_page.url)
        WebDriverWait(login_browser, BETWEEN_PAGE_WAIT).until(PageDirectEvent(lgn_page.url))
        usrname_tbox = login_browser.find_element(By.XPATH, lgn_page.username_input_xpath)
        usrname_tbox.send_keys(self.login_data.username)
        password_tbx = login_browser.find_element(By.XPATH, lgn_page.password_input_xpath)
        password_tbx.send_keys(self.login_data.password)
        sbmt_btn_xpath = lgn_page.submit_button_xpath
        submit_on_tbx = not sbmt_btn_xpath

        if sbmt_btn_xpath:
            login_browser.find_element(By.XPATH, lgn_page.submit_button_xpath).click()

            try:
                WebDriverWait(login_browser, BETWEEN_PAGE_WAIT).until(PageChangeEvent(lgn_page.url))
            except TimeoutException as e:
                scripter_log.error("Timeout by Failed Login. {} doesn't change.".format(login_browser.current_url))
                scripter_log.error(e)
                submit_on_tbx = True

        if submit_on_tbx:
            password_tbx.submit()

        if save_login_data:
            scripter_log.warn("Saving is not implemented yet for new CRSCrawler. Use LegacyCRSCrawler")

    @classmethod
    def update_bookings(cls, start_date, end_date):
        raise NotImplementedError


class SabreCrawler(CRSCrawler):
    login_data = secrets.SabreLoginData
    crs_profile = sabre.SabreCRSProfile()

    BOOKING_LIST_LOAD_TIMEOUT = 20
    BOOKING_LIST_FILTER_LOAD_TIMEOUT = 20
    BOOKING_LIST_DETAILS_LOAD_TIMEOUT = 20

    def get_details(self, brsr, url):
        deets_page = self.crs_profile.detail_page
        WebDriverWait(brsr, self.BOOKING_LIST_DETAILS_LOAD_TIMEOUT)\
            .until(ElementExistsEvent(deets_page.lighthouse_element))
        for name, field_data in deets_page.get_fields_xpath_pairs():
            ele_index = 0
            if field_data.secondary_xpath:
                for i, ele in enumerate(brsr.find_elements(By.XPATH, field_data.secondary_xpath[0])):
                    if ele.text == field_data.secondary_xpath[1]:
                        ele_index = i
                        break
            try:
                raw_value = brsr.find_elements(By.XPATH, field_data.xpath)[ele_index].text
                if field_data.delimiter and field_data.index:
                    if ":" in field_data.index:
                        start, end = map(int, field_data.index.split(":"))
                    else:
                        start = int(field_data.index)
                        end = None
                    field_data.value = " ".join(raw_value.split(field_data.delimiter)[start: end])
                else:
                    field_data.value = raw_value
            except (InvalidSelectorException, IndexError):
                if field_data.xpath == '{{url}}':
                    field_data.value = url
                else:
                    scripter_log.error("Invalid Selector: {}".format(field_data.xpath))
            except NoSuchElementException:
                scripter_log.warning("Can't find xpath for field, {}, using {}".format(name, field_data.xpath))
        self.loader.load_booking(deets_page)

    def traverse_booking_list(self, brsr, start_date=None, end_date=None):
        bk_tbl_pg = self.crs_profile.booking_table_page

        fixed_strt_dt = start_date
        if not isinstance(start_date, str):
            if not start_date:
                fixed_strt_dt = bk_tbl_pg.min_date or datetime.min
            fixed_strt_dt = fixed_strt_dt.strftime(bk_tbl_pg.filter_date_format)

        fixed_end_dt = end_date
        if not isinstance(end_date, str):
            if not end_date:
                fixed_end_dt = bk_tbl_pg.max_date or datetime.max
            fixed_end_dt = fixed_end_dt.strftime(bk_tbl_pg.filter_date_format)

        for filter_start_date, filter_end_date in \
                get_month_date_ranges(fixed_strt_dt, fixed_end_dt, bk_tbl_pg.filter_date_format):
            brsr.get(bk_tbl_pg.url)
            WebDriverWait(brsr, self.BOOKING_LIST_LOAD_TIMEOUT).until(PageDirectEvent(bk_tbl_pg.url))
            brsr.find_element(By.XPATH, bk_tbl_pg.filter_bking_opt_xpath).click()
            WebDriverWait(brsr, self.BOOKING_LIST_FILTER_LOAD_TIMEOUT)\
            .until(ElementExistsEvent(bk_tbl_pg.filter_start_date_tbx_xpath))
            st_tbx = brsr.find_element(By.XPATH, bk_tbl_pg.filter_start_date_tbx_xpath)
            st_tbx.clear()
            st_tbx.send_keys(filter_start_date)
            end_tbx = brsr.find_element(By.XPATH, bk_tbl_pg.filter_end_date_tbx_xpath)
            end_tbx.clear()
            end_tbx.send_keys(filter_end_date)
            brsr.find_element(By.XPATH, bk_tbl_pg.filter_submit_btn_xpath).click()
            WebDriverWait(brsr, self.BOOKING_LIST_FILTER_LOAD_TIMEOUT) \
                .until(ElementExistsEvent(bk_tbl_pg.res_table_xpath))
            links = []
            for i in range(bk_tbl_pg.res_start_indx, bk_tbl_pg.max_res_per_page):
                res_xpath = bk_tbl_pg.res_links_xpath_pattern.format(i)
                try:
                    links.append(brsr.find_element(By.XPATH, res_xpath).get_attribute('href'))
                except NoSuchElementException:
                    scripter_log.error("Element @ {} doesn't exist".format(res_xpath))
                    break
            for lk in links:
                brsr.get(lk)
                WebDriverWait(brsr, self.BOOKING_LIST_LOAD_TIMEOUT).until(PageDirectEvent(lk))
                self.get_details(brsr, lk)

    def update_bookings(self, start_date=None, end_date=None):
        brsr = browser_class()
        self.login(brsr)
        self.traverse_booking_list(brsr, start_date, end_date)
        brsr.quit()
