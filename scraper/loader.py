import re
from datetime import datetime
from booking.models import *
from formgen.data.common import FormatType, DetailPageInfo
from utils.date import format_string_to_date, format_string_to_datetime

SOURCE = "CRS Updater"
APP_LABEL = "booking"

ROOM_DICT = {'DPVT': 'DPVT',
             'Double with Private Bath': 'DPVT',
             'Double with Private Bath(DPVT)': 'DPVT',
             'Double Shared Bath': 'D',
             'Double Shared Bath(D)': 'D',
             'Double Deluxe Shared Bathroom': 'DD',
             'Double Deluxe Shared Bathroom(DD)': 'DD',
             'Queen Shared Bathroom': 'DD',
             'Q': 'DD',
             'Basic Double': 'BD',
             'Basic Double Shared Bathroom': 'BD',
             'Basic Double Shared Bathroom(BD)': 'BD',
             'Classic Single': 'CS',
             'Classic Single with Shared Bathroom(CS)': 'CS',
             'Basic Single': 'BS',
             'BS': 'BS',
             'Basic Single with Shared Bathroom': 'BS',
             'Basic Single with Shared Bathroom(BS)': 'BS',
             'Single Deluxe': 'Single with Priv',
             'Single Deluxe with Private Bathroom': 'Single with Priv',
             'Classic Single with Shared Bathroom': 'Single with Priv',
             'Single Deluxe with Private Bathroom(Sgl w Priv)': 'Single with Priv',
             '3PersonBunkbed': 'Dbl/SglBunkbed/s',
             '3PersonBunkbed with Shared Bath': 'Dbl/SglBunkbed/s',
             '3PersonBunkbed with Shared Bath(DblSglBunk)': 'Dbl/SglBunkbed/s'}


def get_room_type(crs_data):
    return RoomType.objects.get(abbreviation=ROOM_DICT[crs_data])


class QuickChangeType:
    Cancelled = BookingChangeType.objects.get(name=BookingChangeType.Names.CANCELLED)
    New = BookingChangeType.objects.get(name=BookingChangeType.Names.NEW)
    Duplicate = BookingChangeType.objects.get(name=BookingChangeType.Names.DUPLICATE)


class BookingLoader:
    def load_booking(self, booking):
        raise NotImplementedError


class ResMatrixBookingLoader(BookingLoader):
    skippable_fields = []
    date_format = None
    datetime_format = None
    crs = None

    def __init__(self, datetime_format=None, date_format=None, crs_name=None):
        super().__init__()
        self.date_format = date_format
        self.datetime_format = datetime_format
        self.crs = CRS()
        self.crs.name = crs_name
        self.crs.save()

    def load_booking(self, booking):
        gst = Guest()
        gst.first_name = gst.booking_first_name = booking.get('first_name', '')
        gst.last_name = gst.booking_last_name = booking.get('last_name', '')
        gst.email = booking.get('Email', '')
        gst.city = booking.get('city', '')
        gst.street = booking.get('street', '')
        gst.country = booking.get('country', '')
        gst.comment = ""
        gst.save()
        pn = PhoneNumber()
        pn.phone_number_guest = gst
        pn.number = booking.get('phone_number', '')
        pn.save()
        bk = Booking(booking_guest=Guest.objects.get(id=gst.id))
        bk.check_in = format_string_to_date(booking.get('date_in', ''), self.date_format)
        bk.check_out = format_string_to_date(booking.get('date_out', ''), self.date_format)
        bk.book_date = format_string_to_datetime(booking.get('Book Date', ''), self.datetime_format)
        bk.booking_crs = self.crs
        bk.crs_comment = booking.get('Guest Comment', '')
        bk.crs_id = booking.get('pk', '')
        bk.channel = booking.get('channel', '')
        bk.email_opt_in = booking.get('Email Opt In', '') == 'Yes'
        bk.payment_method = booking.get('Guest Method', '')
        bk.status = 2
        qtys = booking.get('Rm/Ad/Ch', '').split("/")
        bk.room_qty = qtys[0]
        bk.adult_guest_qty = qtys[1]
        bk.child_quest_qty = qtys[2]
        # TODO: re update tax code until 2015-10-22
        bk.tax_code = float(booking.get('room_taxes', '').strip('$'))
        bk.price_code = float(booking.get('room_rate', '').strip('$'))
        rm_type = booking.get('Room Type', None)
        bk.preferred_room_type = self.get_room_type(rm_type)
        bk.save()
        bk_hist = BookingHistory()
        bk_hist.hist_booking = bk
        bk_hist.source = SOURCE
        bk_hist.date_changed = datetime.now()
        bk_hist.change_type = BkChangeType.NEW
        bk_hist.save()


class DjangoBookingLoader(BookingLoader):
    skippable_fields = []
    date_format = None
    datetime_format = None
    crs = None
    status_dict = None

    def __init__(self, crs_name, status_dict=None):
        super().__init__()
        if CRS.objects.filter(name=crs_name).exists():
            self.crs = CRS.objects.filter(name=crs_name)[0]
        else:
            self.crs = CRS()
            self.crs.name = crs_name
            self.crs.save()
        self.status_dict = status_dict

    def populate_model(self, model_type, field_dict, crs_field=None):
        mdl = model_type()

        if crs_field:
            setattr(mdl, crs_field, self.crs)

        for mdl_field in field_dict.values():
            if mdl_field.format_type == FormatType.DATETIME:
                db_value = datetime.strptime(mdl_field.value, mdl_field.format)
            elif mdl_field.format_type == FormatType.RE:
                db_value = re.search(mdl_field.value, mdl_field.format).group(0)
            else:
                db_value = mdl_field.value
            if mdl_field.data_type == datetime:
                db_value = db_value
            elif mdl_field.data_type == "RoomType" or mdl_field.data_type == RoomType:
                db_value = get_room_type(mdl_field.value)
            elif mdl_field.data_type == "BookingChangeType" or mdl_field.data_type == BookingChangeType:
                db_value = BookingStatus.objects.get(name=mdl_field.value)
            elif self.status_dict and mdl_field.data_type == BookingStatus:
                db_value = self.status_dict[mdl_field.value]
            else:
                if mdl_field.data_type != str:
                    # TODO: Add log if data_type is unknown
                    pass
                db_value = mdl_field.data_type(mdl_field.value)

            setattr(mdl, mdl_field.field, db_value)
        return mdl

    def load_booking(self, booking_data: DetailPageInfo):
        guest = self.populate_model(Guest, booking_data.guest_fields)
        cancelled_status = BookingStatus(name=BookingStatus.Names.CANCELLED)

        if guest.email and \
                Guest.objects.filter(first_name=guest.first_name,
                                     last_name=guest.last_name,
                                     email=guest.email).exists():
            # TODO: Resolve Conflicting Data Here
            guest = Guest.objects.get(first_name=guest.first_name, last_name=guest.last_name, email=guest.email)
        else:
            guest.save()
        for pn_field in booking_data.phone_number_field_list:
            phone_number = self.populate_model(PhoneNumber, pn_field)
            if phone_number.number:
                phone_number.phone_number_guest = guest
                phone_number.save()

        booking = self.populate_model(Booking, booking_data.booking_fields, 'booking_crs')
        booking.booking_guest = guest
        bk_hist = BookingHistory()
        bk_hist.hist_booking = str(booking)
        bk_hist.source = SOURCE
        bk_hist.date_changed = datetime.now()

        dupe_bookings = Booking.objects.filter(check_in=booking.check_in,
                                               check_out=booking.check_out,
                                               booking_guest__last_name=guest.last_name,
                                               booking_guest__first_name=guest.first_name,
                                               preferred_room_type=booking.preferred_room_type)
        if dupe_bookings:
            if dupe_bookings.filter(status=cancelled_status).exists():
                bk_hist.notes = str(map(str, dupe_bookings))
                bk_hist.change_type = QuickChangeType.Cancelled
            else:
                bk_hist.change_type = QuickChangeType.Duplicate
            bk_hist.booking_id = dupe_bookings[0].id
        else:
            bk_hist.change_type = QuickChangeType.New
            booking.save()
            bk_hist.booking_id = booking.id
        bk_hist.save()

