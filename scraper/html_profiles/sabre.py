import sys
from datetime import datetime
from booking.models import BookingStatus
from formgen.data.common import CRSProfile, BookingTableInfo, LoginPageInfo, DetailPageInfo, BookingField, FormatType


class SabreLoginPageInfo(LoginPageInfo):
    url = "https://reservations.synxis.com/CC/login.aspx"
    username_input_xpath = "//input[@id='LoginCntrl_UsernameTextBox']"
    password_input_xpath = "//input[@id='LoginCntrl_PasswordTextBox']"
    submit_button_xpath = "//input[@id='LoginCntrl_LoginButton']"


class SabreBookingTableInfo(BookingTableInfo):
    url = "https://reservations.synxis.com/CC/Reservation/ReservationSearch.aspx?pageNavId=201088"
    filter_bking_opt_xpath = "//option[@value='bookingActivityDateRange']"
    filter_start_date_tbx_xpath = "//input[@id='ctl00_cph_RezSearchControl_BookDateRangeP" \
                                  "icker_DatePickerControl1_DateTextBox']"
    filter_end_date_tbx_xpath = "//input[@id='ctl00_cph_RezSearchControl_BookDateRangePic" \
                                "ker_DatePickerControl2_DateTextBox']"
    filter_submit_btn_xpath = "//input[@id='ctl00_cph_RezSearchControl_SearchDate']"
    res_start_indx = 2
    max_res_per_page = sys.maxsize
    res_table_xpath = "//table[@id='ctl00_cph_FoundRezList_ReservationListDataGrid']"
    res_links_xpath_pattern = "//a[@id='ctl00_cph_FoundRezList_ReservationListDataGrid_ctl{:02d}_OpenRezDetails']"
    min_date = datetime(2015, 1, 1)
    max_date = None
    filter_date_format = "%m/%d/%Y"

    def __init__(self):
        self.max_date = datetime.now()


class SabreDetailPageInfo(DetailPageInfo):
    lighthouse_element = "//div[@id='ContentContainer']"
    phone_number_qty = 2
    check_date_format = "%A, %B %d, %Y"
    book_date_format = "%m/%d/%Y %I:%M %p"
    CUST_INFO_DIV_ID = "ctl00_cph_RezListRepeater_ctl00_RezDetailControl_UpdatePanelCust"
    RES_INFO_DIV_ID = 'ctl00_cph_RezListRepeater_ctl00_RezDetailControl_ResInfo'

    status_dict = {
        BookingStatus.Names.CANCELLED: "cancelled",
        BookingStatus.Names.CONFIRMED: "confirmed"
    }

    def __init__(self):
        super().__init__()

        self.add_guest_field(BookingField(str, 'Guest', 'first_name',
                                          "//span[@id='ctl00_cph_RezListRepeater_ctl00_RezDetailControl_"
                                          "GuestRepeater_ctl00_GuestNameHeaderLabel']", delimiter=" ", index="0:-1"))
        self.add_guest_field(BookingField(str, 'Guest', 'last_name',
                                          "//span[@id='ctl00_cph_RezListRepeater_ctl00_RezDetailControl_"
                                          "GuestRepeater_ctl00_GuestNameHeaderLabel']", delimiter=" ", index="-1"))
        self.add_guest_field(BookingField(str, 'Guest', 'email',
                                          "//tr/td[contains(text(), 'Primary Email')]/following-sibling::td"))
        self.add_guest_field(BookingField(str, 'Guest', 'city',
                                          "//tr/td[contains(text(), 'City')]/following-sibling::td"))
        self.add_guest_field(self.build_row_field(str, 'Guest', 'street', 'Address 1'))
        self.add_guest_field(self.build_row_field(str, 'Guest', 'email', "Email:"))
        self.add_guest_field(self.build_row_field(str, 'Guest', 'country', 'Country:'))
        self.add_guest_field(BookingField(str, 'Guest', 'booking_first_name',
                                          "//span[@id='ctl00_cph_RezListRepeater_ctl00_RezDetailControl_"
                                          "GuestRepeater_ctl00_GuestNameHeaderLabel']"))

        self.add_phone_number(self.build_row_field(str, 'PhoneNumber-1', 'number', 'Mobile Phone'))
        self.add_phone_number(self.build_row_field(str, 'PhoneNumber-2', 'number', 'Phone:',
                                                   page_div=self.CUST_INFO_DIV_ID))

        chk_in_field = self.build_row_field(datetime, 'Booking', 'check_in', 'Arrival Date')
        chk_in_field.format = self.check_date_format
        chk_in_field.format_type = FormatType.DATETIME
        self.add_booking_field(chk_in_field)
        chk_out_field = self.build_row_field(datetime, 'Booking', 'check_out', 'Departure Date')
        chk_out_field.format = self.check_date_format
        chk_out_field.format_type = FormatType.DATETIME
        self.add_booking_field(chk_out_field)
        self.add_booking_field(BookingField(str, 'Booking', 'url', "{{url}}"))
        bk_date_field = self.build_row_field(datetime, 'Booking', 'book_date', 'Book Date')
        bk_date_field.format = self.book_date_format
        bk_date_field.format_type = FormatType.DATETIME
        self.add_booking_field(bk_date_field)
        self.add_booking_field(self.build_row_field(str, 'Booking', 'crs_id', 'Confirmation Number:',
                                                    page_div=self.RES_INFO_DIV_ID))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'channel_id', 'Channel confirmation number'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'payment_method', 'Payment Type'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'channel', "Channel:",
                                                    page_div=self.RES_INFO_DIV_ID))
        self.add_booking_field(self.build_row_field("RoomType", 'Booking', 'preferred_room_type', 'Room (Code)'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'cancellation_policies', 'Cancellation Policy'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'adult_guest_qty', 'Number of Adults'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'child_quest_qty', 'Number of Children'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'tax_code', 'Total Taxes, fees and surcharges'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'price_code', 'Reservation Total'))
        self.add_booking_field(self.build_row_field("BookingChangeType", 'Booking', 'status', 'Reservation Status'))
        self.add_booking_field(self.build_row_field(int, 'Booking', 'number_of_nights', 'Number of Nights'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'room_code', 'Room (Code)'))
        self.add_booking_field(self.build_row_field(int, 'Booking', 'room_qty', 'Number of Rooms'))
        self.add_booking_field(self.build_row_field(str, 'Booking', 'crs_comment', 'Comment'))

    @staticmethod
    def build_row_field(field_type, model, field, label, page_div=None):
        if page_div:
            output = BookingField(field_type, model, field,
                                  "//table/tbody/tr/td[contains(text(), '{}')]//following-sibling::td".format(label),
                                  secondary_xpath=("//div[@id='{}']//tr"
                                                   "/td[contains(text(), '{}')]".format(page_div, label),
                                                   label))
        else:
            output = BookingField(field_type, model, field,
                                  "//table/tbody/tr/td[contains(text(), '{}')]//following-sibling::td".format(label))
        return output


class SabreCRSProfile(CRSProfile):
    root_url = "https://reservations.synxis.com/"
    unimportant_fields = []

    def __init__(self):
        super().__init__()
        self.login_page = SabreLoginPageInfo()
        self.booking_table_page = SabreBookingTableInfo()
        self.detail_page = SabreDetailPageInfo()
