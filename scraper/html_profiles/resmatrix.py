from formgen.data.common import CRSProfile


class ResMatrixCRSProfile(CRSProfile):
    def __init__(self):
        super().__init__()
        self.html_pages['Itinerary'] = [('#CPH1_spResvID', 'pk'),
                                        ('#CPH1_spChannel', 'channel'),
                                        ('#CPH1_spCheckIn', 'date_in'),
                                        ('#CPH1_spCheckOut', 'date_out'),
                                        ('#CPH1_spAdultsChildren', 'Rm/Ad/Ch'),
                                        ('#CPH1_spRoomType', 'Room Type'),
                                        ('#CPH1_spBookDate', 'Book Date'),
                                        ('#CPH1_spRoomAmount', 'room_rate'),
                                        ('#CPH1_spRoomTaxes', 'room_taxes')]

        self.html_pages['Guest Info'] = [('#CPH1_spFirstname', "first_name"),
                                         ('#CPH1_spLastname', "last_name"),
                                         ('#CPH1_spAddress', "street"),
                                         ('#CPH1_spCity', "city"),
                                         ('#CPH1_spState', "state"),
                                         ('#CPH1_spCountry', "country"),
                                         ('#CPH1_spEmail', "Email"),
                                         ('#CPH1_spPhone', "phone_number")]
