from django import template
from booking.db.readers import UpdateDropboxReader

register = template.Library()


@register.assignment_tag()
def get_dropbox_info():
    reader = UpdateDropboxReader()
    reader.sync_with_db()
    return reader.info
