from django.conf.urls import url
from booking import views, ajax_views

urlpatterns = [
    url(r'main_menu/', views.main_menu, name='main_menu'),
    url(r'room_assignment/', views.room_assignment, name='room_assignment'),
    url(r'availability/', views.availability, name='availability'),
    url(r'crs_update/', ajax_views.update_bookings, name='crs_update'),
    url(r'direct_guests/', views.direct_guests, name='direct_guests'),
    url(r'booking/details/(?P<booking_id>[0-9]+)', views.booking_details, name='booking_details'),
    url(r'booking/details/printable/(?P<booking_id>[0-9]+)', views.printable_booking_details, name='prnt_book_details'),
]
