import sys
import traceback
from datetime import datetime
from django.contrib import messages
from django.http import HttpResponseBadRequest, HttpResponse
from lbs import settings
from lbs.states import cwlr_state, CrawlerStatus
from scraper.crawler import SabreCrawler
from scraper.loader import DjangoBookingLoader
from secrets import MASTER_PASSWORD
from .db.writers import UpdaterWriter
from .db.readers import UpdaterReader

BAD_PASSWORD_MESSAGE = 'Bad Password for CRS Booking Update. Please Try Again.'
CRAWLING_IN_PROGRESS_MSG = "Can't update again CRS Booking Updating is Already in Progress"
crwl_success_msg_tmplt = "Successfully added {} booking(s)."

cwlr_state.crawler_status = CrawlerStatus.IDLE


def update_bookings(request):
    pw = request.POST.get('password', '')
    UpdaterWriter().record_scrapper_history(datetime.now(), request.user)
    if cwlr_state.crawler_status == CrawlerStatus.IDLE:
        if MASTER_PASSWORD == pw:
            crawler = SabreCrawler(DjangoBookingLoader("Sabre"))
            output = None
            try:
                change_crawler_status(CrawlerStatus.CRAWLING)
                reader = UpdaterReader()
                reader.sync_with_db()
                latest_db_bookdate = reader.latest_booking
                crawler.update_bookings(latest_db_bookdate)
            except Exception as e:
                if settings.DEBUG:
                    output = HttpResponseBadRequest(str(e))
                    print(e)
                    ex_type, ex, tb = sys.exc_info()
                    traceback.print_tb(tb)
            else:
                output = HttpResponse()
            finally:
                change_crawler_status(CrawlerStatus.IDLE)
                return output
        else:
            messages.add_message(request, messages.ERROR, BAD_PASSWORD_MESSAGE)
            return HttpResponseBadRequest(BAD_PASSWORD_MESSAGE)
    else:
        messages.add_message(request, messages.ERROR, CRAWLING_IN_PROGRESS_MSG)
        return HttpResponseBadRequest(CRAWLING_IN_PROGRESS_MSG)


def change_crawler_status(cs):
    cwlr_state.crawler_status = cs
