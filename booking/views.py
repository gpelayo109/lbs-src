import os
from io import BytesIO
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.utils.timezone import datetime
from lbs.settings import BASE_DIR
from lbs.states import cwlr_state, CrawlerStatus
from utils.date import DATE_FORMAT
from .db.readers import LoginContextReader, BookingDetailsContextReader, PrintableBookDeetsContextReader, MainPageContextReader, \
    RoomAssignmentContextReader, AvailabilitySheetContextReader, DirectGuestsContextReader
from .db.writers import RoomAssignmentWriter
from .models import DoubleBookingError


class Status:
    BAD_LOGIN = 0
    USER_IS_NOT_ACTIVE = 1


def logout_view(request):
    logout(request)
    return HttpResponseRedirect("/login/")


def login_view(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/lbs/main_menu')

    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'login.html')

    cxt = LoginContextReader()
    if request.method == 'GET':
        context = cxt.build_context_dict()
        return render(request, template, context)
    elif request.method == 'POST':
        usr = request.POST['username']
        pw = request.POST['password']
        logged_usr = authenticate(username=usr, password=pw)

        if not logged_usr:
            cxt.set_message("Login information is incorrect. Please Try Again")
            return render(request, template, cxt.build_context_dict())
        elif not logged_usr.is_active:
            cxt.set_message("User is not activated. Please contact the admin.")
            return render(request, template, cxt.build_context_dict())
        else:
            logout(request)
            login(request, logged_usr)
            return HttpResponseRedirect('/lbs/main_menu')

CRAWLER_IN_PROGRESS_MSG = "System is currently updating booking from the internet. Booking info may change."


def has_server_messages(function):
    def func_wrapper(request, *args, **kwargs):
        if cwlr_state.crawler_status == CrawlerStatus.CRAWLING:
            messages.add_message(request, messages.WARNING, CRAWLER_IN_PROGRESS_MSG)
        return function(request, *args, **kwargs)
    return func_wrapper


@has_server_messages
@login_required
def booking_details(request, booking_id):
    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'booking_details.html')
    cxt = BookingDetailsContextReader(booking_id)
    cxt.sync_with_db()
    return render(request, template, cxt.build_context_dict())


@login_required
def printable_booking_details(request, booking_id):
    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'printable_page.html')
    cxt = PrintableBookDeetsContextReader(booking_id)
    cxt.sync_with_db()
    return render(request, template, cxt.build_context_dict())


@has_server_messages
@login_required
def main_menu(request):
    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'main_menu.html')
    cxt = MainPageContextReader()
    cxt.sync_with_db()
    return render(request, template, cxt.build_context_dict())


SEARCH_SUBMIT = 'search'
DATE_FILTER_SUBMIT = 'guest_date'

@has_server_messages
@login_required
def room_assignment(request):
    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'room_assignment.html')
    guest_date = request.GET.get(DATE_FILTER_SUBMIT, None) or datetime.today().strftime(DATE_FORMAT)

    cxt = RoomAssignmentContextReader(guest_date)
    if request.method == 'POST':
        if 'assign' == request.POST['post-type']:
            try:
                editor = RoomAssignmentWriter()
                editor.assign_room(request.POST['id'], request.POST['room_num'])
            except DoubleBookingError as e:
                cxt.set_message(str(e))
    cxt.sync_with_db()
    data_args = cxt.build_context_dict()
    return render(request, template, context=data_args)


FIRST_NAME = 'first_name'
LAST_NAME = 'last_name'


@has_server_messages
@login_required
def direct_guests(request):
    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'direct_guests.html')

    page_num = request.GET.get('page', None)
    if not page_num:
        page_num = 1
    else:
        page_num = int(page_num)

    # first_name = request.GET.get(FIRST_NAME, None)
    # last_name = request.GET.get(LAST_NAME, None)

    cxt = DirectGuestsContextReader(page_num)
    cxt.sync_with_db()
    data_args = cxt.build_context_dict()
    return render(request, template, context=data_args)

@has_server_messages
@login_required
def availability(request):
    template = os.path.join(BASE_DIR, 'templates', 'lbs', 'availability.html')
    data_args = AvailabilitySheetContextReader().build_context_dict()

    if 'guest_date' in request.GET:
        guest_date = request.GET['guest_date']
        sheet_file = BytesIO()
        AvailabilitySheetDecorator.get_assignment_sheet(sheet_file, guest_date)
        res = HttpResponse(sheet_file.getvalue(), content_type="application/application/vnd.open"
                                                               "xmlformats-officedocument.spreadsheetml.sheet")
        res['Content-Disposition'] = "attachment; filename={}".format('{} - Availability.xls'.format(guest_date))
        res['Content-length'] = sheet_file.tell()
        return res
    else:
        return render(request, template, context=data_args)


@login_required
def root_redirect(request):
    return HttpResponseRedirect('/lbs/main_menu')
