from datetime import datetime
from django import forms
from utils.date import DATE_FORMAT


class UpcomingGuestFilterForm(forms.Form):
    guest_date = forms.DateField(required=False, label='Date', widget=forms.TextInput(attrs={"class": 'dateinput'}))


class DirectGuestFilterForm(forms.Form):
    guest_date = forms.DateField(required=False, label='Name', widget=forms.TextInput())


class AvailabilitySheetForm(forms.Form):
    guest_date = forms.DateField(required=True, initial=datetime.today().strftime(DATE_FORMAT),
                                 label='Start Date', widget=forms.TextInput(attrs={"class": 'dateinput'}))


class RequestUpdateForm(forms.Form):
    master_password = forms.CharField(label="password", widget=forms.PasswordInput())


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())
