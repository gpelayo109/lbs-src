from . import _bookkeepers as _bk


class RoomAssignmentWriter:
    @staticmethod
    def assign_room(booking_id, room_num):
        _bk.RoomAssignmentBookkeeper.assign_room(booking_id, room_num)


class UpdaterWriter:
    @staticmethod
    def record_scrapper_history(date_started, user):
        _bk.UpdaterBookkeeper().save_scrapper_history(date_started, user)
