from booking import page_codes
from booking.forms import *
from . import _bookkeepers as _bk


class Reader:
    def sync_with_db(self):
        pass


class UpdateDropboxReader:
    info = None

    def sync_with_db(self):
        self.info = _bk.UpdateDropboxBookkeeper().get_info()


class UpdaterReader():
    latest_booking = None

    def sync_with_db(self):
        self.latest_booking = _bk.UpdaterBookkeeper().get_latest_booking_date()


class AvailabilityExcelSheetReader:
    def __init__(self, start_date, end_date=None):
        self.start_date = start_date
        self.end_date = end_date
        self.guest_list = None

    def sync_with_db(self):
        keeper = _bk.AvailabilitySheetBookkeeper(self.start_date, self.end_date)
        self.guest_list = keeper.get_guest_list()


class ContextReader(Reader):
    message = None

    def set_message(self, msg):
        self.message = msg

    def build_context_dict(self):
        raise NotImplementedError


class AvailabilitySheetContextReader(ContextReader):
    date = None

    def __init__(self, start_date: datetime = datetime.today()):
        self.date = start_date

    def build_context_dict(self):
        filter_form = AvailabilitySheetForm()

        data_args = {
            'filter': filter_form,
            'message': self.message,
            'page_code': 'aval'
        }

        return data_args


class LoginContextReader(ContextReader):
    login_form = LoginForm()

    def build_context_dict(self):
        return {
            'form': self.login_form,
            'message': self.message
        }


class MainPageContextReader(ContextReader):
    table_rows = None
    page_code = page_codes.MAIN_PAGE
    last_updated = None

    def sync_with_db(self):
        editor = _bk.MainPageBookkeeper()
        self.table_rows = editor.get_table()

    def build_context_dict(self):
        return {
            'table_data': self.table_rows,
            'page_code': self.page_code,
            'update_form': RequestUpdateForm()
        }


class BookingDetailsContextReader(ContextReader):
    booking_details = None
    guest_details = None
    booking_id = None

    def __init__(self, booking_id):
        self.booking_id = booking_id

    def sync_with_db(self):
        hndlr = _bk.BookingDetailsBookkeeper(self.booking_id)
        self.booking_details, self.guest_details = hndlr.get_details()

    def build_context_dict(self):
        return {
            'booking_id': self.booking_id,
            'booking_details': self.booking_details,
            'guest_details': self.guest_details
        }


class PrintableBookDeetsContextReader(ContextReader):
    sections = None
    last_page = None
    booking_id = None

    def __init__(self, booking_id):
        self.booking_id = booking_id
        self.last_page = None

    def sync_with_db(self):
        hndlr = _bk.BookingDetailsBookkeeper(self.booking_id)
        self.sections = hndlr.get_printable_details()

    def build_context_dict(self):
        return {
            'booking_id': self.booking_id,
            'sections': self.sections
        }


class RoomAssignmentContextReader(ContextReader):
    def __init__(self, initial_date):
        self.message = ""
        self.table_data = []
        self.initial_start_date = initial_date

    def sync_with_db(self):
        hndlr = _bk.RoomAssignmentBookkeeper(self.initial_start_date)
        self.table_data = hndlr.build_table()

    def build_context_dict(self):
        filter_form = UpcomingGuestFilterForm(initial={'guest_date': self.initial_start_date})

        data_args = {
            'filter': filter_form,
            'table_data': self.table_data,
            'message': self.message,
            'page_code': "rmas"
        }

        return data_args


class DirectGuestsContextReader(ContextReader):
    def __init__(self, page_num, first_name=None, last_name=None):
        # TODO replace settings with db accessors once settings is setup
        self.first_name = first_name
        self.last_name = last_name
        self.size = 40
        self.page_num = page_num
        self.offset = (page_num - 1) * self.size
        self.message = ""
        self.table_data = []
        self.pager_data = []

    def sync_with_db(self):
        hndlr = _bk.DirectGuestBookkeeper(self.size, self.offset)
        self.table_data = hndlr.build_table()
        self.pager_data = hndlr.build_pager_data()

    def build_context_dict(self):
        filter_form = DirectGuestFilterForm()
        data_args = {
            'filter': filter_form,
            'table_data': self.table_data,
            'message': self.message,
            'pager_data': self.pager_data,
            'page_code': "drgt",
            'current_page': self.page_num,
        }
        return data_args