import math
from booking.models import Booking, RoomAssignment, RoomType, Room
from utils.date import format_to_django_date, MAX_DJGO_DATE, MIN_DJGO_DATE


def get_booking_by_checkin(date):
    return Booking.objects.filter(check_in=date).order_by('check_in')


def get_room_number_by_crs_id(crs_id):
    if RoomAssignment.objects.filter(current_booking__crs_id=crs_id).exists():
        return RoomAssignment.objects.get(current_booking__crs_id=crs_id).assigned_room.room_number
    else:
        return


def get_bookings_staying_at_date(date, end_date=None):
    fmt_dt = format_to_django_date(date)
    fmt_out_dt = format_to_django_date(date, offset=1)
    if not end_date:
        min_checkin = MIN_DJGO_DATE
        max_checkin = fmt_dt
        min_checkout = fmt_out_dt
        max_checkout = MAX_DJGO_DATE
    else:
        fmt_end_dt = format_to_django_date(end_date)
        min_checkin = MIN_DJGO_DATE
        max_checkin = fmt_end_dt
        min_checkout = fmt_out_dt
        max_checkout = MAX_DJGO_DATE
    result = Booking.objects.filter(check_in__range=[min_checkin, max_checkin],
                                    check_out__range=[min_checkout, max_checkout]).order_by('check_in')
    return result


class RoomCompatibility:
    SAME = 0
    COMPATIBLE = 1
    NOT_COMPATIBLE = 2


class RoomChoice:
    room_number = None
    compatibility = None


# TODO change to get_room_compatibility list
def get_rooms(room_type: RoomType):
    room_list = []
    for room in Room.objects.all():
        rc = RoomChoice()
        rc.room_number = room.room_number
        if room.room_type in room_type.alternate_room_types.all():
            rc.compatibility = RoomCompatibility.COMPATIBLE
        elif room.room_type == room_type:
            rc.compatibility = RoomCompatibility.SAME
        else:
            rc.compatibility = RoomCompatibility.NOT_COMPATIBLE
        room_list.append(rc)

    return room_list


DIRECT_BOOKING_CHANNEL = "Booking Engine (BBE)"
CONFIRMED_STATUS_NAME = "Confirmed"


def get_direct_bookings(size, offset=0):
    return Booking.objects.filter(channel=DIRECT_BOOKING_CHANNEL,
                                  status__name=CONFIRMED_STATUS_NAME).order_by('-check_in')[offset: offset + size]


def get_direct_bk_ttl_pg_num(size):
    return math.ceil(Booking.objects.filter(channel=DIRECT_BOOKING_CHANNEL,
                                            status__name=CONFIRMED_STATUS_NAME).count()/size)
