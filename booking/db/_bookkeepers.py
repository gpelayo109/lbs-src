from datetime import datetime
from booking.models import *
from ._shortcuts import *


class MainPageBookkeeper:
    class MainPageBookingData:
        name = None
        street = None
        city = None
        state = None
        country = None
        date_in = None
        date_out = None
        phone_number = None
        room_number = None
        booking_id = None

    def __init__(self, date=datetime.today()):
        self.date = date

    def get_table(self):
        bookings = get_booking_by_checkin(self.date)
        table_rows = []

        for r in bookings:
            bd = self.MainPageBookingData()
            guest = r.booking_guest
            bd.name = "{} {}".format(guest.first_name, guest.last_name)
            bd.city = guest.city
            bd.street = guest.street
            bd.state = ""
            bd.country = guest.country
            bd.phone_number = [pn.number for pn in PhoneNumber.objects.filter(phone_number_guest=guest)]
            bd.email = guest.email
            bd.date_in = r.check_in
            bd.date_out = r.check_out
            bd.booking_id = r.pk
            bd.room_number = get_room_number_by_crs_id(r.crs_id)
            table_rows.append(bd)

        return table_rows

BOOKING_FIELDS = ['booking_guest', 'check_in', 'check_out', 'book_date', 'booking_crs',
                  'crs_id', 'channel_id', 'channel', 'room_qty', 'preferred_room_type', 'adult_guest_qty',
                  'child_quest_qty', 'tax_code', 'price_code', 'price_id', 'status']

GUEST_FIELDS = ['first_name', 'last_name', 'email', 'street', 'city', 'country', 'zip_code']


class BookingDetailsBookkeeper:
    class DetailLine:
        name = None
        value = None

    class DetailPageData:
        fields = None
        name = None

        def __init__(self, name, fields):
            self.name = name
            self.fields = fields

    class PrintableSection:
        name = None
        lines = None

        def __init__(self, name):
            self.name = name

    def __init__(self, booking_id):
        self.booking_id = booking_id

    def __build_details(self, model_instance, field_names):
        fields = []
        for fd in field_names:
            dt = self.DetailLine()
            dt.value = getattr(model_instance, fd)
            dt.name = model_instance._meta.get_field(fd).verbose_name.title()
            fields.append(dt)
        return self.DetailPageData(str(model_instance), fields)

    def get_details(self):
        booking = Booking.objects.get(pk=self.booking_id)
        booking_details = self.__build_details(booking, BOOKING_FIELDS)

        for detail in booking_details.fields:
            if isinstance(detail.value, Guest):
                guest_details = self.__build_details(detail.value, GUEST_FIELDS)
                break
        else:
            guest_details = None

        return booking_details, guest_details

    def get_printable_details(self):
        booking = Booking.objects.get(pk=self.booking_id)
        booking_section = self.PrintableSection("Booking Information")
        bk_dict = self.__build_details(booking, BOOKING_FIELDS)
        booking_section.lines = ["{}:  {}".format(bk.name, bk.value) for bk in bk_dict.fields]
        guest_section = None

        for detail in bk_dict.fields:
            if isinstance(detail.value, Guest):
                guest_section = self.PrintableSection("Guest Information")
                guest_section.lines = ["{}:  {}".format(gst.name, gst.value)
                                       for gst in self.__build_details(detail.value, GUEST_FIELDS).fields]
                break
        sections = []
        if guest_section:
            sections.append(guest_section)
        sections.append(booking_section)

        return sections


# TODO change to DisplayBooking
class UpcomingBooking:
    name = None
    crs_id = None
    check_in = None
    check_out = None
    room_type = None
    room_number = None
    possible_rooms = None
    date_booked = None


class RoomAssignmentBookkeeper:
    def __init__(self, date=datetime.today()):
        self.date = date

    @staticmethod
    def assign_room(booking_id, room_num):
        new_booking = Booking.objects.get(id=booking_id)
        if room_num:
            new_room = Room.objects.get(room_number=room_num)
            if RoomAssignment.objects.filter(current_booking=new_booking).exists():
                ra = RoomAssignment.objects.get(current_booking=new_booking)
            else:
                ra = RoomAssignment()
            ra.current_booking = new_booking
            ra.assigned_room = new_room
            ra.date_assigned = datetime.today()
            ra.clean()
            ra.save()
        else:
            rm_a = RoomAssignment.objects.get(current_booking=new_booking)
            rm_a.delete()

    def build_table(self):
        db_data = get_bookings_staying_at_date(self.date)
        table_data = []

        for r in db_data:
            row = UpcomingBooking()
            row.name = "{} {}".format(r.booking_guest.first_name, r.booking_guest.last_name)
            row.check_in = r.check_in
            row.check_out = r.check_out
            row.id = r.id
            if RoomAssignment.objects.filter(current_booking__id=row.id).exists():
                row.room_number = RoomAssignment.objects.get(current_booking__id=row.id).assigned_room.room_number
            row.room_type = r.preferred_room_type
            row.possible_rooms = get_rooms(r.preferred_room_type)
            table_data.append(row)
        return table_data


class DirectGuestBookkeeper:
    def __init__(self, size, offset=0):
        self.size = size
        self.offset = offset

    def build_table(self):
        db_data = get_direct_bookings(self.size, offset=self.offset)
        table_data = []
        for r in db_data:
            row = UpcomingBooking()
            row.name = "{} {}".format(r.booking_guest.first_name, r.booking_guest.last_name)
            row.check_in = r.check_in
            row.check_out = r.check_out
            row.id = r.id
            row.date_booked = r.book_date.strftime("%b. %d, %Y")
            if RoomAssignment.objects.filter(current_booking__id=row.id).exists():
                row.room_number = RoomAssignment.objects.get(current_booking__id=row.id).assigned_room.room_number
            row.room_type = r.preferred_room_type
            table_data.append(row)
        return table_data

    def build_pager_data(self):
        return [i+1 for i in range(get_direct_bk_ttl_pg_num(self.size))]


class DailyLogExcelBookkeeper:
    def build_guest_list(self, start_date, end_date=None):
        ral = get_bookings_staying_at_date(start_date, end_date=end_date)
        guest_list = []
        for r in ral:
            row = UpcomingBooking()
            row.name = "{} {}".format(r.booking_guest.first_name, r.booking_guest.last_name)
            row.check_in = r.check_in
            row.check_out = r.check_out
            row.crs_id = r.crs_id
            row.room_type = r.preferred_room_type
            row.possible_rooms = get_rooms(r.preferred_room_type)
            row.room_number = get_room_number_by_crs_id(r.crs_id)
            guest_list.append(row)


class FieldNames:
    BOOKING_DATE = 'book_date'
    HISTORY_CHANGE_DATE = 'date_changed'


class ChangeTypes:
    NEW = 'New'
    CHANGED = 'Guest Info Modified'
    CANCELLED = 'Cancelled'
    DUPLICATE = 'Duplicate'


class DropDownData:
    last_update_date = None
    new_qty = None
    change_qty = None
    cancelled_qty = None


class UpdateDropboxBookkeeper:
    def get_info(self):
        last_update_date = getattr(BookingHistory.objects.all().latest(FieldNames.HISTORY_CHANGE_DATE),
                                   FieldNames.HISTORY_CHANGE_DATE)
        new_id = BookingChangeType.objects.filter(name=ChangeTypes.NEW)
        changed_id = BookingChangeType.objects.filter(name=ChangeTypes.CHANGED)
        cancelled_id = BookingChangeType.objects.filter(name=ChangeTypes.CANCELLED)
        last_update_changes = BookingHistory.objects.filter(date_changed=last_update_date)
        new_qty = last_update_changes.filter(change_type=new_id).count()
        changed_qty = last_update_changes.filter(change_type=changed_id).count()
        cancelled_qty = last_update_changes.filter(change_type=cancelled_id).count()

        dta = DropDownData()
        dta.new_qty = new_qty
        dta.cancelled_qty = cancelled_qty
        dta.change_qty = changed_qty
        dta.last_update_date = last_update_date

        return dta


BOOKING_DATE_FIELD_NAME = 'book_date'


class UpdaterBookkeeper:
    def get_latest_booking_date(self):
        return Booking.objects.all().latest(BOOKING_DATE_FIELD_NAME).book_date

    def save_scrapper_history(self, date_started, user):
        update_hist = ScrapperUpdateHistoy()
        update_hist.date_updated = datetime.now()
        update_hist.triggered_by = user
        update_hist.save()


# DEFAULT_DAILY_LOG_MAX_DAYS = 7
# OCCUPIED_SYM = "X"
# MAX_NAME_LENGTH = 10


class AvailabilitySheetBookkeeper:
    def __init__(self, start_date, end_date=None):
        self.start_date = start_date
        self.end_date = end_date

    def get_guest_list(self):
        booking = get_bookings_staying_at_date(self.start_date, end_date=self.end_date)
        guest_list = []
        for r in booking:
            row = UpcomingBooking()
            row.name = "{} {}".format(r.booking_guest.first_name, r.booking_guest.last_name)
            row.check_in = r.check_in
            row.check_out = r.check_out
            row.crs_id = r.crs_id
            if RoomAssignment.objects.filter(current_booking__crs_id=r.crs_id).exists():
                row.room_number = RoomAssignment.objects.get(current_booking__crs_id=r.crs_id).assigned_room.room_number
            row.room_type = r.preferred_room_type
            row.possible_rooms = get_rooms(r.preferred_room_type)
            guest_list.append(row)
        return guest_list
