import os
import configparser
from django.contrib.auth.models import User
from django.test import LiveServerTestCase, Client

from selenium.webdriver import Chrome, ChromeOptions
from booking import APP_DIR

test_download_folder = os.path.join(APP_DIR, 'test', 'downloads')
DEFAULT_IP = 'http://127.0.0.1:8000'

if not os.path.exists(test_download_folder):
    os.mkdir(test_download_folder)

cfg = configparser.ConfigParser()
cf_ip_address = cfg.get('Django', 'ip', fallback=DEFAULT_IP)


class MockUserFactory:
    name = "Test"
    eml = "TestEmail"
    pw = "MonkeyPoof"
    user = None

    def create(self):
        self.user = User.objects.create_user(self.name, self.eml, password=self.pw)
        return self.user

    def delete(self):
        self.user.delete()


class LoginTest(LiveServerTestCase):
    browser = None
    test_usr = None
    co = ChromeOptions()
    usr = MockUserFactory()
    # desired_caps = {'download.default_directory': test_download_folder}
    # co.add_experimental_option('pref', desired_caps)

    global cf_ip_address
    site_address = cf_ip_address

    def setUp(self):
        self.usr.create()
        self.browser = Chrome(chrome_options=self.co)
        self.browser.get(self.site_address)
        super(LiveServerTestCase, self).setUp()

    def test_login(self):
        usr_elmt = self.browser.find_element_by_id("id_username")
        usr_elmt.send_keys(self.usr.name)
        pass_elmt = self.browser.find_element_by_id("id_password")
        pass_elmt.send_keys(self.usr.pw)
        login_btn = self.browser.find_element_by_id("login_btn")
        login_btn.click()

    def tearDown(self):
        self.usr.delete()
        super(LiveServerTestCase, self).tearDown()