from datetime import datetime
from django.contrib import admin
from .models import *


class PhoneNumberInline(admin.StackedInline):
    model = PhoneNumber
    fk_name = 'phone_number_guest'


class GuestAdmin(admin.ModelAdmin):
    search_fields = ['first_name', 'last_name']
    inlines = [PhoneNumberInline, ]

SOURCE = "Admin"


class BookingAdmin(admin.ModelAdmin):
    def delete_model(self, request, queryset):
        for mdl in queryset:
            bk_hist = BookingHistory()
            bk_hist.hist_booking = mdl
            bk_hist.source = SOURCE
            bk_hist.date_changed = datetime.now()
            bk_hist.change_type = BookingChangeType.Names.GUEST_INFO_CHANGED
            bk_hist.save()
            mdl.delete()

    search_fields = ['booking_guest__first_name', 'booking_guest__last_name', 'crs_id']


admin.site.register(Booking, BookingAdmin)
admin.site.register(RoomType)
admin.site.register(RoomOption)
admin.site.register(BedType)
admin.site.register(Guest, GuestAdmin)
admin.site.register(Room)
admin.site.register(RoomAssignment)
admin.site.register(CRS)
admin.site.register(BookingHistory)
admin.site.register(BookingStatus)
admin.site.register(PhoneNumber)
admin.site.register(BookingChangeType)
admin.site.register(ScrapperUpdateHistoy)
