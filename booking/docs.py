from datetime import timedelta, datetime
from utils.date import format_to_datetime
from excel.generator import AvailabilitySheetGenerator
from .db.readers import AvailabilityExcelSheetReader

DAYS = 7
ROOMS = [1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 22, 23]


class AvailabilitySheetDecorator:
    @staticmethod
    def get_assignment_sheet(file, start_date=datetime.today()):
        if isinstance(start_date, str):
            start_date = format_to_datetime(start_date)
        end_date = start_date + timedelta(days=DAYS)
        reader = AvailabilityExcelSheetReader(start_date, end_date)
        reader.sync_with_db()
        guest_list = reader.guest_list

        asg = AvailabilitySheetGenerator(file, start_date)
        asg.set_displayable_room_nums(ROOMS)
        asg.add_guest_list(guest_list)
        asg.save()
