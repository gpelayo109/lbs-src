from django.db import models
from django.contrib.auth import models as authmdl
from utils.date import format_to_django_date, MAX_DJGO_DATE, MIN_DJGO_DATE


class DoubleBookingError(Exception):
    pass


class LuzHotelModel(models.Model):
    pass


class Position:
    name = models.CharField(max_length=35)


# class Employee:
#     first_name = models.CharField(max_length=35)
#     last_name = models.CharField(max_length=35)
#     position = models.ForeignKey(max_length=35)
#     date_joined = models.DateField()
#     contact_number = models.CharField(max_length=35)
#     address = models.CharField(max_length=300)
#     email = models.CharField(max_length=35)
#
#
# class CheckInRecord:
#     front_desk = models.ForeignKey(Employee)
#     assigned_booking = models.ForeignKey(RoomAssignment)
#

class RoomOption(LuzHotelModel):
    name = models.CharField(max_length=35)

    def __str__(self):
        return self.name


class BedType(LuzHotelModel):
    name = models.CharField(max_length=35)
    capacity = models.IntegerField()

    def __str__(self):
        return self.name


class BookingStatus(LuzHotelModel):
    class Names:
        CANCELLED = 'Cancelled'
        CONFIRMED = 'Confirmed'

    name = models.CharField(max_length=35,
                            choices=[(Names.CONFIRMED, Names.CONFIRMED),
                                     (Names.CANCELLED, Names.CANCELLED)])

    def __str__(self):
        return self.name


class BookingChangeType(LuzHotelModel):
    class Names:
        NEW = "New"
        GUEST_INFO_CHANGED = "Guest Info Modified"
        CANCELLED = "Cancelled"
        DUPLICATE = "Duplicate"

    name = models.CharField(choices=[(Names.NEW, Names.NEW),
                                     (Names.GUEST_INFO_CHANGED, Names.GUEST_INFO_CHANGED),
                                     (Names.CANCELLED, Names.CANCELLED),
                                     (Names.DUPLICATE, Names.DUPLICATE)],
                            max_length=35)

    def __str__(self):
        return self.name


class RoomType(LuzHotelModel):
    name = models.CharField(max_length=35)
    abbreviation = models.CharField(max_length=35)
    options = models.ManyToManyField('RoomOption')
    bed_type = models.ForeignKey('BedType')
    alternate_room_types = models.ManyToManyField('RoomType')

    def __str__(self):
        return "[{}] {}".format(self.abbreviation, self.name)


class Room(LuzHotelModel):
    room_number = models.CharField(max_length=10, unique=True)
    room_type = models.ForeignKey('RoomType')
    comment = models.CharField(max_length=10, blank=True)

    def __str__(self):
        return "{}{}".format(self.room_number, self.room_type.abbreviation)


class RoomAssignment(LuzHotelModel):
    date_assigned = models.DateField()
    current_booking = models.ForeignKey('Booking')
    assigned_room = models.ForeignKey('Room')
    check_in = models.DateField()
    check_out = models.DateField()

    def __str__(self):
        return "[{}] {}".format(self.assigned_room, self.current_booking.booking_guest)

    def clean(self):
        if not self.check_in:
            self.check_in = self.current_booking.check_in
        if not self.check_out:
            self.check_out = self.current_booking.check_out

        chk_in = format_to_django_date(self.check_in)
        chk_out = format_to_django_date(self.check_out)
        conflicts = None
        if self.assigned_room:
            conflicts = RoomAssignment.objects.filter(assigned_room=self.assigned_room, check_in__range=[chk_in, chk_out])
            if not conflicts:
                conflicts = RoomAssignment.objects.filter(assigned_room=self.assigned_room,
                                                          check_out__range=[chk_in, chk_out])
                if not conflicts:
                    conflicts = RoomAssignment.objects.filter(assigned_room=self.assigned_room,
                                                              check_in__range=[MIN_DJGO_DATE, chk_out],
                                                              check_out__range=[chk_in, MAX_DJGO_DATE])
        if conflicts:
            raise DoubleBookingError('Room Assignment conflicts with Booking: {}'.format(conflicts[0].current_booking))


class Guest(LuzHotelModel):
    first_name = models.CharField(max_length=35, blank=True)
    last_name = models.CharField(max_length=35, blank=True)
    email = models.CharField(max_length=35, blank=True)
    city = models.CharField(max_length=35, blank=True)
    street = models.CharField(max_length=140, blank=True)
    country = models.CharField(max_length=10, blank=True)
    comment = models.CharField(max_length=500, blank=True)
    zip_code = models.CharField(max_length=35, blank=True)
    booking_first_name = models.CharField(max_length=35, blank=True)
    booking_last_name = models.CharField(max_length=35, blank=True)

    def __str__(self):
        return "{}, {}".format(self.last_name, self.first_name)


class PhoneNumber(LuzHotelModel):
    phone_number_guest = models.ForeignKey(Guest, verbose_name="Guest")
    number = models.CharField(max_length=35)


class CRS(LuzHotelModel):
    name = models.CharField(max_length=35)

    def __str__(self):
        return self.name


class Booking(LuzHotelModel):
    booking_guest = models.ForeignKey(Guest, null=True, blank=True)
    check_in = models.DateField()
    check_out = models.DateField()
    url = models.CharField(max_length=500, blank=True)
    book_date = models.DateTimeField()
    booking_crs = models.ForeignKey(CRS, verbose_name="Central Reservation System", blank=True, null=True)
    crs_comment = models.CharField(max_length=500, blank=True)
    crs_id = models.CharField(max_length=35, blank=True)
    channel_id = models.CharField(max_length=35, blank=True)
    payment_method = models.CharField(max_length=70)
    channel = models.CharField(max_length=35)
    cancellation_policies = models.CharField(max_length=100, blank=True)
    room_qty = models.IntegerField()
    preferred_room_type = models.ForeignKey(RoomType, blank=True)
    adult_guest_qty = models.IntegerField()
    child_quest_qty = models.IntegerField(blank=True)
    tax_code = models.CharField(max_length=64, verbose_name="Tax")
    price_code = models.CharField(max_length=64, verbose_name="Reservation Total")
    price_id = models.CharField(max_length=64)
    status = models.ForeignKey(BookingStatus)

    def __str__(self):
        return "{} - {} ({})".format(self.check_in, self.booking_guest, self.crs_id)


class BookingHistory(LuzHotelModel):
    hist_booking = models.CharField(max_length=250)
    booking_id = models.IntegerField()
    source = models.CharField(max_length=35)
    date_changed = models.DateField()
    change_type = models.ForeignKey(BookingChangeType)
    notes = models.CharField(max_length=300, blank=True, null=True)

    def __str__(self):
        return "[{}] {}".format(self.change_type, self.hist_booking)


class ScrapperUpdateHistoy(LuzHotelModel):
    date_updated = models.DateTimeField()
    triggered_by = models.ForeignKey(authmdl.User)
