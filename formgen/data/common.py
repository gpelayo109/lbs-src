class FormatType:
    RE = 0
    DATETIME = 1


class BookingField:
    format = None
    format_type = None
    value = None

    def __init__(self, data_type, model, field_name, xpath,
                 secondary_xpath=None,
                 delimiter=None,
                 index=None):
        self.delimiter = delimiter
        self.index = index
        self.data_type = data_type
        self.model = model
        self.field = field_name
        self.xpath = xpath
        self.secondary_xpath = secondary_xpath

    def set_format(self, format_type: FormatType, field_format):
        self.format = field_format
        self.format_type = format_type

    def __str__(self):
        return "{}.{}".format(self.model, self.field)


class BookingData:
    guest = None
    booking = None
    phone_numbers = None

    def __init__(self):
        self.phone_numbers = []


class BookingTableInfo:
    url = None
    booking_table_page = None
    min_date = None
    max_date = None
    filter_date_format = None


class LoginPageInfo:
    url = None
    submit_button_xpath = None
    username_input_xpath = None
    password_input_xpath = None


class DetailPageInfo:
    url_pattern = None
    phone_number_qty = 0
    booking_fields = None
    guest_fields = None
    phone_number_field_list = None

    def __init__(self):
        self.booking_fields = {}
        self.guest_fields = {}
        self.phone_number_field_list = []

    def add_booking_field(self, field_data: BookingField):
        self.booking_fields["{}.{}".format(field_data.model, field_data.field)] = field_data

    def add_guest_field(self, field_data: BookingField):
        self.guest_fields["{}.{}".format(field_data.model, field_data.field)] = field_data

    def add_phone_number(self, field_data: BookingField):
        self.phone_number_field_list.append({"{}.{}".format(field_data.model, field_data.field): field_data})

    def get_fields_xpath_pairs(self):
        for name, xpath in self.booking_fields.items():
            yield name, xpath

        for name, xpath in self.guest_fields.items():
            yield name, xpath

        for pn in self.phone_number_field_list:
            for name, xpath in pn.items():
                yield name, xpath


class SessionInfo:
    session_filename = None


class CRSProfile:
    login_page = None
    booking_table_page = None
    detail_page = None
    unimportant_fields = None
    session_info = None
