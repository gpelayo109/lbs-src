class CrawlerStateManager:
    crawler_status = None


class CrawlerStatus:
    IDLE = 0
    CRAWLING = 1
    RESTING = 2


cwlr_state = CrawlerStateManager()
