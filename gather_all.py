from formgen.data.api.resmatrix.gaddery import ResMatrixBookingGaddery

START_DATE = "1/1/2004"
END_DATE = "1/1/2018"

gd = ResMatrixBookingGaddery(START_DATE, END_DATE)
bookings = gd.collect_data()
gd.close()
