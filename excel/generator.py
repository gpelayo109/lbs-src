import os
import copy
import xlwt
from xlwt import Style as WtStyle
from datetime import datetime, timedelta
import xlrd
from xlutils import copy as xlcopy
from lbs.settings import BASE_DIR


as_base_style = xlwt.XFStyle()
as_base_style.num_format_str = "MM/DD/YYYY"
alnmnt = xlwt.Alignment()
alnmnt.horz = xlwt.Alignment.HORZ_CENTER
as_base_style.alignment = alnmnt

as_date_style = copy.copy(as_base_style)
brdr = xlwt.Borders()
brdr.top = brdr.right = brdr.left = xlwt.Borders.THIN
as_date_style.borders = brdr

as_name_style = copy.copy(as_base_style)
brdr = xlwt.Borders()
brdr.top = xlwt.Borders.DASHED
brdr.right = brdr.bottom = brdr.left = xlwt.Borders.THIN
brdr.bottom_colour = WtStyle.colour_map['white']
as_name_style.borders = brdr

as_mark_style = copy.copy(as_base_style)
brdr = xlwt.Borders()
brdr.right = brdr.left = xlwt.Borders.THIN
brdr.bottom = xlwt.Borders.DASHED
brdr.top_colour = WtStyle.colour_map['white']
fnt = xlwt.Font()
fnt.bold = True
fnt.height = 24 * 20
fnt.colour_index = WtStyle.colour_map['black']
as_mark_style.font = fnt
as_mark_style.borders = brdr


class ExcelGenerator:
    tplt_path = None
    template = None
    xl_instance = None

DAYS = 7
ROOMS = [1, 3, 4, 5, 6, 7, 8, 9, 10, 12, 22, 23]


class AvailabilitySheetGenerator(ExcelGenerator):
    DATE_LOC = ((1, 3), (7, 3))
    TOP_LEFT_CONTENT_LOC = (1, 5)
    MAX_NAME_LENGTH = 15
    MAX_CONTENT_COL = 7
    OCCUPIED_SYM = "X"
    tplt_path = os.path.join(BASE_DIR, 'excel', 'templates', 'availability.xlt')
    template = xlrd.open_workbook(tplt_path, formatting_info=True)
    room_assignments = None
    displayable_rooms = None
    start_date = None

    def __init__(self, file, start_date=datetime.today()):
        self.start_date = start_date
        self.file = file
        self.xl_instance = xlcopy.copy(self.template)
        self.room_assignments = []
        self.displayable_rooms = dict()
        sheet = self.xl_instance.get_sheet(0)

        for y in range(self.DATE_LOC[0][1], self.DATE_LOC[1][1] + 1):
            for x in range(self.DATE_LOC[0][0], self.DATE_LOC[1][0] + 1):
                col_date = start_date + timedelta(days=(x - self.DATE_LOC[0][0]))
                sheet.write(y, x, col_date, as_date_style)

    def set_displayable_room_nums(self, num_list):
        self.displayable_rooms.clear()
        for i, n in enumerate(num_list):
            self.displayable_rooms[str(n)] = i

    def add_guest_list(self, room_assignment_list):
        for rm in room_assignment_list:
            self.room_assignments.append(rm)

    def save(self):
        # TODO set_col_dynamically
        for rm in self.room_assignments:
            if rm.room_number in self.displayable_rooms.keys():
                row_offset = self.displayable_rooms[rm.room_number]
                col_offset = (datetime.combine(rm.check_in, datetime.min.time()) - self.start_date).days
                row = self.TOP_LEFT_CONTENT_LOC[1] + row_offset*2
                column = self.TOP_LEFT_CONTENT_LOC[0] + col_offset if col_offset >= 0 else self.TOP_LEFT_CONTENT_LOC[0]
                sheet = self.xl_instance.get_sheet(0)

                if len(rm.name) > self.MAX_NAME_LENGTH:
                    display_name = rm.name[0: self.MAX_NAME_LENGTH-1].strip() + "..."
                else:
                    display_name = rm.name

                sheet.write(row, column, display_name, as_name_style)
                days = (rm.check_out - rm.check_in).days + col_offset

                for x in range(days):
                    if column + x > self.MAX_CONTENT_COL:
                        break
                    sheet.write(row + 1, column + x, self.OCCUPIED_SYM, as_mark_style)
        self.xl_instance.save(self.file)


