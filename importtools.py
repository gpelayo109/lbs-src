from booking.models import *
from django.utils.timezone import datetime
import pytz
import json
from utils.sec import generate_salt, generate_hash
from tqdm import tqdm

room_dict = {'DPVT': 'DPVT',
             'Double with Private Bath': 'DPVT',
             'Double Shared Bath': 'D',
             'Double Deluxe Shared Bathroom': 'DD',
             'Queen Shared Bathroom': 'DD',
             'Q': 'DD',
             'Basic Double': 'BD',
             'Basic Double Shared Bathroom': 'BD',
             'Classic Single': 'CS',
             'Basic Single': 'BS',
             'BS': 'BS',
             'Basic Single with Shared Bathroom': 'BS',
             'Single Deluxe': 'Single with Priv',
             'Single Deluxe with Private Bathroom': 'Single with Priv',
             'Classic Single with Shared Bathroom': 'Single with Priv',
             '3PersonBunkbed': 'Dbl/SglBunkbed/s',
             '3PersonBunkbed with Shared Bath': 'Dbl/SglBunkbed/s'}


def get_room_type(hms_data):
    return RoomType.objects.get(abbreviation=room_dict[hms_data])


RESMATRIX_DATE = "%m/%d/%Y"
RESMATRIX_DATETIME = "%m/%d/%Y %I:%M:%S %p"


def format_date(date_str):
    return get_datetime(date_str, RESMATRIX_DATE)


def format_datetime(date_str):
    return get_datetime(date_str, RESMATRIX_DATETIME)


def get_datetime(date_str, date_format):
    naive_datetime = datetime.strptime(date_str, date_format)
    return pytz.timezone("UTC").localize(naive_datetime)


def load():
    with open('data-dump.json', 'r') as dmp:
        reader = json.load(dmp)
        hms = CRS()
        hms.name = "ResMatrix"
        hms.save()
        for booking in tqdm(reader):
            gst = Guest()
            gst.first_name = booking.get('first_name', '')
            gst.last_name = booking.get('last_name', '')
            gst.email = booking.get('Email', '')
            gst.city = booking.get('city', '')
            gst.street = booking.get('street', '')
            gst.country = booking.get('country', '')
            gst.comment = ""
            gst.save()
            pn = PhoneNumber()
            pn.phone_number_guest = gst
            pn.number = booking.get('phone_number', '')
            pn.save()
            bk = Booking(booking_guest=Guest.objects.get(id=gst.id))
            bk.check_in = format_date(booking.get('date_in', ''))
            bk.check_out = format_date(booking.get('date_out', ''))
            bk.book_date = format_datetime(booking.get('Book Date', ''))
            bk.booking_hms = hms
            bk.hms_comment = booking.get('Guest Comment', '')
            bk.hms_id = booking.get('pk', '')
            bk.channel = booking.get('channel', '')
            bk.email_opt_in = booking.get('Email Opt In', '') == 'Yes'
            bk.payment_method = booking.get('Guest Method', '')
            qtys = booking.get('Rm/Ad/Ch', '').split("/")
            bk.room_qty = qtys[0]
            bk.adult_guest_qty = qtys[1]
            bk.child_quest_qty = qtys[2]
            bk.price_id = generate_salt(5)
            bk.tax_code = generate_hash(float(booking.get('room_rate', '').strip('$')), bk.price_id)
            bk.price_code = generate_hash(float(booking.get('room_rate', '').strip('$')), bk.price_id)
            rm_type = booking.get('Room Type', None)
            bk.preferred_room_type = get_room_type(rm_type)
            bk.save()


def nuke():
    sec_phrase = input('Type Security Phrase: ')
    if sec_phrase == 'Kill them all Peter':
        print('So be it...may God forgive us.')
        Booking.objects.all().delete()
        Guest.objects.all().delete()
        PhoneNumber.objects.all().delete()
        CRS.objects.all().delete()
        print('The damage has been done.')
    else:
        print('Bad Phrase. Nuke Disarmed')
