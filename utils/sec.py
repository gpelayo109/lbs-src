import string
from random import Random
import hashlib

salt_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits


def generate_salt(char_qty):
    r = Random()
    salt = ""
    for i in range(char_qty):
        salt += r.choice(salt_chars)
    return salt


def generate_hash(data, salt):
    return hashlib.sha256((str(data) + str(salt)).encode('utf-8')).hexdigest()
