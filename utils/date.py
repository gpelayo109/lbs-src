from datetime import datetime, timedelta
from lbs.settings import TIME_ZONE
import pytz
import calendar

DATE_FORMAT = '%m/%d/%y'
ALT_DATE_FORMAT = '%m/%d/%Y'

DJANGO_FORMAT = '%Y-%m-%d'


def format_to_django_date(date, offset=0):
    raw_date = date

    if isinstance(date, str):
        try:
            raw_date = datetime.strptime(date, DATE_FORMAT)
        except ValueError:
            raw_date = datetime.strptime(date, ALT_DATE_FORMAT)

    raw_date += timedelta(days=offset)

    print(raw_date.strftime(DJANGO_FORMAT))
    return raw_date.strftime(DJANGO_FORMAT)


def format_to_datetime(date):
    try:
        return datetime.strptime(date, DATE_FORMAT)
    except ValueError:
        return datetime.strptime(date, ALT_DATE_FORMAT)

MAX_DJGO_DATE = format_to_django_date(datetime.max)
MIN_DJGO_DATE = format_to_django_date(datetime.min)


def format_string_to_date(date_str, date_format):
    return get_datetime(date_str, date_format)


def format_string_to_datetime(date_str, datetime_format):
    return get_datetime(date_str, datetime_format)


def get_datetime(date_str, date_format):
    naive_datetime = datetime.strptime(date_str, date_format)
    return pytz.timezone(TIME_ZONE).localize(naive_datetime)


def get_month_date_ranges(start_date, end_date, date_format=DATE_FORMAT):
    step_start_date = datetime.strptime(start_date, date_format) if isinstance(start_date, str) else start_date
    final_end_date = datetime.strptime(end_date, date_format) if isinstance(end_date, str) else end_date
    if step_start_date == final_end_date:
        yield (start_date, end_date)
    else:
        while step_start_date < final_end_date:
            fst_dy, lst_dy = calendar.monthrange(step_start_date.year.real, step_start_date.month.real)
            step_end_date = datetime(step_start_date.year, step_start_date.month, lst_dy)
            if final_end_date < step_end_date:
                step_end_date = final_end_date
            str_start = step_start_date.strftime(date_format)
            str_end = step_end_date.strftime(date_format)
            yield (str_start, str_end)
            if step_start_date.month+1 <= 12:
                step_start_date = datetime(step_start_date.year, step_start_date.month + 1, 1)
            else:
                step_start_date = datetime(step_start_date.year+1, 1, 1)